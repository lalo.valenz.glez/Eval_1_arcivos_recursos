package com.example.lalo.eva_1_archivos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvMostrar = (TextView) findViewById(R.id.archivo);
        //abrir archivo
        //necesitamos un objeto tipo inputstream o openFileInput('nombre');
        //InputStream file = getResources().openRawResource(R.raw.archivo);
        InputStream file = null;
        try{
            file = openFileInput("archivo.txt");
        }catch (IOException e){
            e.printStackTrace();
        }
        if(file != null) {
            //leer BYTES
            InputStreamReader isr = new InputStreamReader(file);
            //LEER TEXTO
            BufferedReader reader = new BufferedReader(isr);
            //leer el archivo
            String text = null;
            try {
                while ((text = reader.readLine()) != null) {
                    tvMostrar.append(text);
                }
                reader.close();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
